from django.contrib import admin

# Register your models here.
from bpm.models import AvailableStatuses, AvailableProcessTask, StatusRoadMap
from service_desk.forms import AvailableProcessTaskForm


class AvailableStatusesInline(admin.TabularInline):
    model = AvailableStatuses


class AvailableStatusesAdmin(admin.ModelAdmin):
    model = AvailableProcessTask
    inlines = (AvailableStatusesInline,)


class AvailableProcessTaskAdmin(admin.ModelAdmin):
    form = AvailableProcessTaskForm
    inlines = (AvailableStatusesInline,)
    readonly_fields = ('process_class', 'process_title', 'task_name', 'task_title')

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        if obj:
            form.base_fields['choice'].initial = "%s.%s" % (obj.process_class, obj.task_name)
        return form

    def save_model(self, request, obj, form, change):
        field = form.fields['choice']
        value = form.cleaned_data['choice']
        obj.process_class, obj.task_name = value.split(".")[:2]
        obj.process_title, obj.task_title = dict(field.choices).get(value, ".").split(".")[:2]
        super().save_model(request, obj, form, change)


class StatusRoadMapAdmin(admin.ModelAdmin):
    list_display = ('task', 'task_status', 'source_status', 'target_status')


admin.site.register(AvailableProcessTask, AvailableProcessTaskAdmin)
admin.site.register(StatusRoadMap, StatusRoadMapAdmin)
