from enum import Enum

from django.db import models
# Create your models here.
from viewflow import STATUS

from core.models import Reference
from django.utils.translation import ugettext_lazy as _


class ViewflowStatusEnumWrapper(STATUS, Enum):
    pass


class Status(Reference):
    pass
    class Meta:
        verbose_name=_('status')
        verbose_name_plural=_('statuses')


class AvailableProcessTask(models.Model):
    """зарегистрированные процесс.задание"""
    process_class = models.CharField(_('process class'),max_length=255, blank=True)
    process_title = models.CharField(_('process title'),max_length=255, blank=True)
    task_name = models.CharField(_('task name'),max_length=255, blank=True)
    task_title = models.CharField(_('task title'),max_length=255, blank=True)

    class Meta:
        unique_together = ('process_class', 'task_name')
        verbose_name=_('available process task')
        verbose_name_plural=_('available process tasks')

    def __str__(self):
        return '%s.%s' % (self.process_title, self.task_title)


class AvailableStatuses(models.Model):
    """доступные статусы для зарегистрированных процесс.задание"""
    task = models.ForeignKey(AvailableProcessTask,verbose_name=_('task'))
    status = models.ForeignKey(Status,verbose_name=_('status'))

    class Meta:
        verbose_name= _('available status')
        verbose_name_plural=_('available statuses')

class StatusRoadMap(models.Model):
    """последовательность автоматической смены статуса документа при изменении статуса процесс.задание"""
    task = models.ForeignKey(AvailableProcessTask,verbose_name=_('task'))
    task_status = models.CharField(_('task status'),max_length=100,
                                   choices=[(k, STATUS.__dict__[k]) for k in STATUS.__dict__ if
                                            not k.startswith('_')])  # viewflow.STATUS
    source_status = models.ForeignKey(Status, related_name='source_status',verbose_name=_('source status'))  # bpm.Status(Reference)
    target_status = models.ForeignKey(Status, related_name='target_status',verbose_name=_('target status'))  # bpm.Status(Reference)

    class Meta:
        verbose_name=_('status road map')
        verbose_name_plural=_('status road maps')