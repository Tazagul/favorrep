"""favorit5 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.views import generic
from material.frontend import urls as frontend_urls
from service_desk import urls as sd_urls

# from core.views import create_custom_model, MyDynRefView
import core.urls as core_urls
from favorit5 import settings

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^core/', include(core_urls)),
    # url(r'^select2/', include('django_select2.urls')),
    url(r'^sd/', include(sd_urls)),

    url(r'^$', generic.RedirectView.as_view(url='/workflow/', permanent=False)),
    url(r'', include(frontend_urls)),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
