import json

import os
from django import http
from django.core import serializers

from django.db import transaction
from django.forms import formset_factory, modelformset_factory, BaseModelFormSet
from django.forms.widgets import HiddenInput
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views.generic.list import ListView
from viewflow.decorators import flow_start_view, flow_view
from viewflow.flow.views import UpdateProcessView
from viewflow.flow.views.utils import get_next_task_url

from bpm.models import Status, AvailableStatuses
from core.models import Branch
from favorit5.settings import BASE_DIR
from service_desk.models import Request, Customer, PerfomanceTest
from . import forms


@flow_start_view
def accessrequest_start_view(request, **kwargs):
    """открытие заявки на доступ"""
    request.activation.prepare(request.POST or None, user=request.user)
    form = forms.AccessRequestForm(request.POST or None)

    if form.is_valid():
        access_request = form.save(commit=False)
        access_request.status = Status.objects.get(code='opened', branch=access_request.branch)
        access_request.taken_by = request.user
        access_request.save()

        request.activation.process.request = access_request
        request.activation.done()

        return redirect(get_next_task_url(request, request.activation.process))

    return render(request,
                  template_name='service_desk/incident/start_flow.html',
                  context={'form': form,
                           'activation': request.activation})


@flow_start_view
def servicerequest_start_view(request, **kwargs):
    """открытие заявки на сервис"""
    request.activation.prepare(request.POST or None, user=request.user)
    form = forms.ServiceRequestForm(request.POST or None)

    if form.is_valid():
        service_request = form.save(commit=False)
        service_request.status = Status.objects.get(code='opened', branch=service_request.branch)
        service_request.taken_by = request.user
        service_request.save()

        request.activation.process.request = service_request
        request.activation.done()

        return redirect(get_next_task_url(request, request.activation.process))

    return render(request,
                  template_name='service_desk/incident/start_flow.html',
                  context={'form': form,
                           'activation': request.activation})


@flow_start_view
def incident_start_view(request, **kwargs):
    """открытие инцидента"""
    request.activation.prepare(request.POST or None, user=request.user)
    form = forms.IncidentForm(request.POST or None)

    if form.is_valid():
        incident = form.save(commit=False)
        incident.status = Status.objects.get(code='opened', branch=incident.branch)
        incident.taken_by = request.user
        incident.save()

        request.activation.process.request = incident
        request.activation.done()

        return redirect(get_next_task_url(request, request.activation.process))

    return render(request,
                  template_name='service_desk/incident/start_flow.html',
                  context={'form': form,
                           'activation': request.activation})


@flow_view
def incident_implementation(request, **kwargs):
    """Выполнение"""
    request.activation.prepare(request.POST or None, user=request.user)
    form = forms.AnswerForm(request.POST or None)
    available_statuses = AvailableStatuses.objects. \
        filter(task__process_class=request.activation.flow_class.instance._meta.flow_label,
               task__task_name=request.activation.flow_task.name).values("status_id")
    form.fields['status'].queryset = Status.objects.filter(id__in=available_statuses)
    form.fields['escalation_level'].widget = HiddenInput()
    if form.is_valid():
        answer = form.save(commit=False)
        answer.request = request.activation.process.request
        answer.request.status = form.cleaned_data.get("status")
        answer.request.save()
        answer.answered_person = request.user
        answer.save()
        request.activation.done()
        return redirect(get_next_task_url(request, request.activation.process))

    add_info = Request.objects.get_subclass(pk=request.activation.process.request.id)
    return render(request,
                  template_name='service_desk/incident/implementation.html',
                  context={'form': form,
                           'activation': request.activation,
                           'add_info': add_info
                           })


@flow_view
def incident_escalation(request, **kwargs):
    """Эскалация"""
    request.activation.prepare(request.POST or None, user=request.user)
    form = forms.AnswerForm(request.POST or None)
    available_statuses = AvailableStatuses.objects. \
        filter(task__process_class=request.activation.flow_class.instance._meta.flow_label,
               task__task_name=request.activation.flow_task.name).values("status_id")
    form.fields['status'].queryset = Status.objects.filter(id__in=available_statuses)

    if form.is_valid():
        answer = form.save(commit=False)
        answer.request = request.activation.process.request
        answer.request.status = form.cleaned_data.get("status", answer.request.status)
        escalation_level = form.cleaned_data.get('escalation_level')
        if escalation_level:
            request.activation.process.escalation_level = escalation_level
            # request.activation.process.save()
        answer.request.save()
        answer.answered_person = request.user
        answer.save()
        request.activation.done()
        return redirect(get_next_task_url(request, request.activation.process))
    else:
        if 'cleaned_data' in form.__dict__.keys():
            status = form.cleaned_data.get("status", Status())
            if status.code == "assigned_to":
                escalation_level = form.cleaned_data.get('escalation_level')
                if escalation_level:
                    request.activation.process.escalation_level = escalation_level
                    request.activation.process.save()
                request.activation.done()
                return redirect(get_next_task_url(request, request.activation.process))
    add_info = Request.objects.get_subclass(pk=request.activation.process.request.id)
    return render(request,
                  template_name='service_desk/incident/implementation.html',
                  context={'form': form,
                           'activation': request.activation,
                           'add_info': add_info
                           })


@flow_view
def incident_close_view(request, **kwargs):
    """Закрытие"""
    request.activation.prepare(request.POST or None, user=request.user)
    form = forms.IncidentCloseForm(request.POST or None)
    available_statuses = AvailableStatuses.objects. \
        filter(task__process_class=request.activation.flow_class.instance._meta.flow_label,
               task__task_name=request.activation.flow_task.name).values("status_id")

    form.fields['status'].choices = [(k['id'], k['name']) for k in
                                     Status.objects.filter(id__in=available_statuses).values('id', 'name').iterator()]

    if form.is_valid():
        request.activation.process.request.status_id = form.cleaned_data['status']
        request.activation.process.request.save()
        request.activation.done()
        return redirect(get_next_task_url(request, request.activation.process))
    add_info = Request.objects.get_subclass(pk=request.activation.process.request.id)
    return render(request,
                  template_name='service_desk/incident/implementation.html',
                  context={'form': form,
                           'activation': request.activation,
                           'add_info': add_info})


class IncidentCloseView(UpdateProcessView):
    """Закрытие"""
    form = forms.IncidentForm


@flow_start_view
def intro_start_view(request, **kwargs):
    request.activation.prepare(request.POST or None, user=request.user)
    form = forms.IntroForm(request.POST or None)

    if form.is_valid():
        intro = form.save(commit=False)
        intro.save()
        request.activation.process.document = intro
        request.activation.done()

        return redirect(get_next_task_url(request, request.activation.process))

    return render(request,
                  template_name='service_desk/intro/start_flow.html',
                  context={'form': form,
                           'activation': request.activation})

@transaction.atomic
def load_customers(request):
    # f = open(os.path.join(BASE_DIR, 'service_desk/fixtures/customers.json'), 'r', encoding='utf-8')
    # obj = json.load(f)
    #
    # for o in obj:
    #     record = Customer()
    #     record.branch = Branch.objects.all()[0]
    #     record.code = o['code']
    #     record.name = o['name']
    #     record.description = ''
    #     record.save()

    return HttpResponse("ok!")


@flow_start_view
def development_start_view(request):
    request.activation.prepare(request.POST or None, user=request.user)
    form = forms.TDAForm(request.POST or None)

    if form.is_valid():
        tda = form.save(commit=False)
        tda.save()
        request.activation.process.document = tda
        request.activation.done()

        return redirect(get_next_task_url(request, request.activation.process))

    return render(request,
                  template_name='service_desk/development/development_start_flow.html',
                  context={'form': form,
                           'activation': request.activation})

@flow_view
def development(request):
    request.activation.prepare(request.POST or None, user=request.user)
    perfomance_tests = request.activation.process.document.perfomancetest_set.all().iterator()
    if request.POST:
        request.activation.done()
        return redirect(get_next_task_url(request, request.activation.process))
    return render(request,
                  template_name='service_desk/development/development.html',
                  context={'activation': request.activation, 'tests': perfomance_tests})

@flow_view
def perfomance_testing(request):
    """тестирование"""
    request.activation.prepare(request.POST or None, user=request.user)
    form = forms.PerfomanceTestForm(request.POST or None, prefix='pt')
    PerfomanceTestingFormset = modelformset_factory(PerfomanceTest,
                                                    form=forms.PerfomanceTestPassForm,
                                                    extra=0)
    if request.POST:
        formset = PerfomanceTestingFormset(request.POST, prefix='ptf')
    else:
        formset = PerfomanceTestingFormset(queryset=request.activation.process.document.perfomancetest_set.all(), prefix='ptf')
    if (form.is_valid() or form.empty_form()) and formset.is_valid():
        if not form.empty_form():
            pt = form.save(commit=False)
            pt.tda = request.activation.process.document
            pt.save()
        formset.save()
        if not form.empty_form() or not len(formset.cleaned_data) == 0:
            if request.POST.get('_continue_testing')==None:
                request.activation.done()
                return redirect(get_next_task_url(request, request.activation.process))
            else:
                request.POST = dict(request.POST)
                request.POST['_continue'] = ''
                return redirect(get_next_task_url(request, request.activation.process))

    return render(request,
                  template_name='service_desk/development/testing.html',
                  context={'form': form,
                           'formset': formset,
                           'activation': request.activation})


class AJAXListMixin(object):

    def dispatch(self, request, *args, **kwargs):
        # if not request.is_ajax():
        #     raise http.Http404("This is an ajax view, friend.")
        return super(AJAXListMixin, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return (
            super(AJAXListMixin, self)
                .get_queryset()
        )

    def get(self, request, *args, **kwargs):
        qs = self.get_queryset();
        result = []
        for c in qs.iterator():
            result.append({'name':c.name, 'pk': c.pk, 'description': c.description, 'code': c.code})
        return http.HttpResponse(json.dumps(result))


class CustomerListView(AJAXListMixin,ListView):
    model = Customer

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['now'] = timezone.now()
        return context
