from django import forms
import bpm.models
from bpm.models import Status
from service_desk.models import Customer, Intro, EscalationLevel
from . import models
from django.utils.translation import ugettext_lazy as _


class RequestForm(forms.ModelForm):
    # customer = forms.ModelChoiceField(required=False, queryset=Customer.objects.all())
    customer_description = forms.CharField(required=False, widget=forms.Textarea,label=_('Customer description'))
    intro = forms.ModelChoiceField(queryset=Intro.objects.filter(answered=False),label=_('Intro'))

    class Meta:
        model = models.Request
        fields = '__all__'
        widgets = {
            'customer': forms.TextInput(attrs={'data-url': '/sd/api/customer/list'}),
        }

    def clean(self):
        intro = self.cleaned_data.get('intro')
        customer = self.cleaned_data.get('customer')
        descr = self.cleaned_data.get('customer_description')
        self.data = self.data.copy()
        if intro and not customer:
            self.cleaned_data['customer'] = intro.customer
            self.data['customer'] = str(intro.customer_id)
        elif not customer:
            self.add_error('customer', 'Обязательное поле или выбирите Обращение')
        if intro and customer and intro.customer != customer:
            self.add_error('customer', 'Клиент должен совпадать с клиентом в Обращение (%s)' % intro.customer)
        if intro and not descr:
            self.cleaned_data['customer_description'] = intro.description
            self.data['customer_description'] = intro.description
        elif not descr:
            self.add_error('customer_description', 'Обязательное поле')
        return self.cleaned_data


class IncidentForm(RequestForm):
    class Meta:
        model = models.Incident
        fields = '__all__'
        exclude = ('status', 'category', 'taken_by', 'number', 'attachments')
        widgets = {
            'customer': forms.TextInput(attrs={'data-url': '/sd/api/customer/list'}),
        }

class IncidentCloseForm(forms.Form):
    status = forms.ChoiceField()
    result = forms.TextInput()


class ServiceRequestForm(RequestForm):
    class Meta:
        model = models.ServiceRequest
        fields = '__all__'
        exclude = ('status', 'category', 'taken_by', 'number', 'attachments')
        widgets = {
            'customer': forms.TextInput(attrs={'data-url': '/sd/api/customer/list'}),
        }


class AccessRequestForm(RequestForm):
    class Meta:
        model = models.AccessRequest
        fields = '__all__'
        exclude = ('status', 'category', 'taken_by', 'number', 'attachments')
        widgets = {
            'customer': forms.TextInput(attrs={'data-url': '/sd/api/customer/list'}),
        }


class IntroForm(forms.ModelForm):
    class Meta:
        model = models.Intro
        fields = '__all__'
        exclude = ('number',)
        widgets = {
            'customer': forms.TextInput(attrs={'data-url': '/sd/api/customer/list'})
        }



class AnswerForm(forms.ModelForm):
    status = forms.ModelChoiceField(queryset=Status.objects.filter(code__in=['solved', 'canceled']))
    escalation_level = forms.ModelChoiceField(queryset=EscalationLevel.objects.all(), required=False)
    class Meta:
        model = models.RequestAnswer
        fields = '__all__'
        exclude = ('request', 'answered_person')


class AvailableProcessTaskForm(forms.ModelForm):
    choice = forms.ChoiceField()

    class Meta:
        model = bpm.models.AvailableProcessTask
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(AvailableProcessTaskForm, self).__init__(*args, **kwargs)
        from django.apps import apps
        cfg = apps.get_app_config('viewflow_frontend')
        __PROCESS_CHOICES = ()
        for flow_meta_class in cfg._registry:
            for node_name in flow_meta_class._meta._nodes_by_name:
                node = flow_meta_class._meta._nodes_by_name[node_name]
                if node.task_type == 'HUMAN':
                    __PROCESS_CHOICES += (("%s.%s" % (flow_meta_class._meta.flow_label, node_name),
                                           "%s.%s" % (flow_meta_class.process_title, node.task_title)),)
        self.fields['choice'].choices = __PROCESS_CHOICES


class TDAForm(forms.ModelForm):
    intro = forms.ModelChoiceField(required=False, queryset=Intro.objects.filter(answered=False),label=_('Intro'))
    class Meta:
        model = models.TDA
        fields = '__all__'
        exclude = ('number',)


class PerfomanceTestForm(forms.ModelForm):
    class Meta:
        model = models.PerfomanceTest
        fields = ('info', 'result', 'passed')
        exclude = ('tda',)

    def empty_form(self):
        if hasattr(self, 'cleaned_data'):
            tmp_info = self.cleaned_data.get('info')
            tmp_result = self.cleaned_data.get('result')
            return (not tmp_info and not tmp_result)
        else:
            return False #нестандартное поведение, т.к. форма еще не проходила валидацию

class PerfomanceTestPassForm(forms.ModelForm):
    # info = forms.CharField(widget=forms.TextInput(attrs={'readonly':'readonly'}))
    # result = forms.CharField(widget=forms.TextInput(attrs={'readonly':'readonly'}))
    class Meta:
        model = models.PerfomanceTest
        fields = '__all__'
        exclude = ('tda', )
        widgets = {
            'passed': forms.CheckboxInput(attrs={'class':'filled-in'})
        }