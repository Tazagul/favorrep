from django.conf.urls import url

from service_desk.views import CustomerListView

urlpatterns = [
    url(r'api/customer/list', CustomerListView.as_view(), name='customer-list'),
]