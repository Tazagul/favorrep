from copy import copy

from viewflow import flow
from viewflow import frontend
from viewflow.base import this, Flow
from viewflow.flow.views import UpdateProcessView
from viewflow.models import Task

import service_desk.views as sd_views
from bpm.models import StatusRoadMap
from .models import IntroProcess, ServiceRequestProcess, AccessRequestProcess, IncidentProcess, DevelopmentProcess, \
    EscalationLevel


class RequestTask(Task):
    def save(self, *args, **kwargs):
        print(args)
        print(kwargs)
        print(self.flow_process)
        print(self.status)
        print(self.token)
        if self.flow_process and self.flow_task:
            print(type(self.flow_task))
            print(self.flow_task.name)
            print(self.flow_task)
            print(self.flow_task.task_type)
            try:
                new_status = StatusRoadMap.objects.get(
                    task__process_class=self.flow_process.flow_class._meta.flow_label,
                    task__task_name=self.flow_task.name,
                    task_status=self.status,
                    source_status=self.flow_process.request.status)
                self.flow_process.request.status = new_status.target_status
                self.flow_process.request.save()
                print("Request was changed status to %s" % new_status.target_status)
            except StatusRoadMap.DoesNotExist:
                pass
        print("---------")

        super(RequestTask, self).save(*args, **kwargs)


def register_flow_steps(flow_class):
    return flow_class


def escalation_level_permission_create(process):
    foo = 1
    return 'service_desk.can_{}_{}{}'.format(process.flow_task.name, process.process._meta.model_name, (
    process.process.escalation_level and '_{}'.format(process.process.escalation_level.code) or ''))


class ExtendedFlowView(flow.View):
    def EscalationLevelBasedPermissions(self, permission=None, auto_create=False, obj=None, help_text=None):
        return super().Permission(permission, auto_create, obj, help_text)

    def ready(self):
        super().ready()
        ###register permissions based on reference for node
            # self.flow_class.process_class._meta.permissions
        el_permisssions = [( 'can_{node_name}_{process_name}'\
                             .format(app=self.flow_class._meta.app_label,
                                     node_name=self.name,
                                     process_name=self.flow_class.process_class._meta.model_name),
                             'Can {} {}'.format(self.task_title, self.flow_class.process_title))]
        try:
            el = EscalationLevel.objects.all()
            for level in el.iterator():
                foo = 1
                permission = 'can_{node_name}_{process_name}_{escalation_level}' \
                    .format(app=self.flow_class._meta.app_label,
                            node_name=self.name,
                            process_name=self.flow_class.process_class._meta.model_name,
                            escalation_level=level.code)
                # permission_text = '{} - {} - {}'.format(self.flow_class.process_class._meta.verbose_name, self.task_title,
                #                                         level.description)
                permission_text = '{}'.format(level.description)
                el_permisssions.append((permission, permission_text))
            #############
        except Exception as e:
            print(e)

        current_perm = self.flow_class.process_class._meta.permissions
        self.flow_class.process_class._meta.permissions = current_perm + tuple(el_permisssions)

    def __init__(self, *args, **kwargs):
        return super().__init__(*args, **kwargs)


@frontend.register
@register_flow_steps
class IncidentFlow(Flow):
    process_class = IncidentProcess
    task_class = RequestTask
    process_title = 'Инцидент'
    process_description = 'Регистрация инцидента для исполнения'
    summary_template = "{{ flow_class.process_title }} {{ process.request.number }} - {{ process.request.status }}"

    start = (
        flow.Start(sd_views.incident_start_view, task_title="Регистрация инцидента")
            .Permission(auto_create=True)
            .Next(this.escalation)
    )

    escalation = (flow.View(sd_views.incident_escalation, task_title='Эскалация')
                  .Permission(auto_create=True)
                  .Next(this.check_answer))

    implementation = ExtendedFlowView(sd_views.incident_implementation, task_title='Выполнение') \
        .EscalationLevelBasedPermissions(escalation_level_permission_create).Next(this.check_answer)
    # fooooo = (flow.View(UpdateProcessView).Permission('can_implementation_incidentprocess_common', help_text='выполнение уровня ВСЕ', auto_create=True),
    #           flow.View(UpdateProcessView).Permission('can_implementation_incidentprocess_developer', help_text='выполнение уровня РАЗРАБОТЧИКИ', auto_create=True),
    #           flow.View(UpdateProcessView).Permission('can_implementation_incidentprocess_methodologist', help_text='выполнение уровня МЕТОДОЛОГИ', auto_create=True)
    #         )
    # implementation = flow.View(sd_views.incident_implementation, task_title='Выполнение') \
    #     .Permission(escalation_level_permission_create) \
    #     .Next(this.check_answer)

    check_answer = (flow.Switch(task_title='Проверка состояния')
                    .Case(this.close, lambda act: mycheck(act, 'solved'))
                    .Case(this.cc_notification, lambda act: mycheck(act, 'canceled'))
                    .Case(this.cc_notification, lambda act: mycheck(act, 'closed'))
                    .Case(this.escalation, lambda act: mycheck(act, 'returned'))
                    .Default(this.implementation))

    close = (flow.View(sd_views.incident_close_view, task_title='Закрытие')
             .Permission(auto_create=True)
             .Next(this.check_answer))

    cc_notification = (flow.Handler(this.cc_notification_handler, task_title='Уведомление').Next(this.end))

    def cc_notification_handler(*args, **kwargs):
        print("Send email ok!")

    end = flow.End()

    def __init__(self):
        foo = 1
        super().__init__()
        foo = 2


@frontend.register
@register_flow_steps
class AccessRequestFlow(Flow):
    process_class = AccessRequestProcess
    task_class = RequestTask
    process_title = 'Предоставление доступа'
    process_description = 'Регистрация заявки на доступ'
    summary_template = "{{ flow_class.process_title }} {{ process.request.number }} - {{ process.request.status }}"

    start = (
        flow.Start(sd_views.accessrequest_start_view, task_title="Регистрация заявки на доступ")
            .Permission(auto_create=True)
            .Next(this.implementation)
    )

    escalation = (flow.View(sd_views.incident_escalation, task_title='Эскалация')
                  .Permission(auto_create=True)
                  .Next(this.check_answer))
    #
    # implementation = flow.View(sd_views.incident_implementation, task_title='Выполнение') \
    #     .Permission('can_implementation_accessrequestprocess_common', help_text='выполнение уровня ВСЕ',
    #                 auto_create=True) \
    #     .Permission('can_implementation_accessrequestprocess_developer', help_text='выполнение уровня РАЗРАБОТЧИКИ',
    #                 auto_create=True) \
    #     .Permission('can_implementation_accessrequestprocess_methodologist', help_text='выполнение уровня МЕТОДОЛОГИ',
    #                 auto_create=True) \
    #     .Permission(escalation_level_permission_create) \
    #     .Next(this.check_answer)
    implementation = ExtendedFlowView(sd_views.incident_implementation, task_title='Выполнение') \
        .EscalationLevelBasedPermissions(escalation_level_permission_create).Next(this.check_answer)

    check_answer = (flow.Switch(task_title='Проверка состояния')
                    .Case(this.close, lambda act: mycheck(act, 'solved'))
                    .Case(this.cc_notification, lambda act: mycheck(act, 'canceled'))
                    .Case(this.cc_notification, lambda act: mycheck(act, 'closed'))
                    .Case(this.escalation, lambda act: mycheck(act, 'returned'))
                    .Default(this.implementation))

    close = (flow.View(sd_views.incident_close_view, task_title='Закрытие')
             .Permission(auto_create=True)
             .Next(this.check_answer))

    cc_notification = (flow.Handler(this.cc_notification_handler, task_title='Уведомление').Next(this.end))

    def cc_notification_handler(*args, **kwargs):
        print("Send email ok!")

    end = flow.End()


@frontend.register
@register_flow_steps
class ServiceRequestFlow(Flow):
    process_class = ServiceRequestProcess
    task_class = RequestTask
    process_title = 'Cервисное обслуживание'
    process_description = 'Регистрация заявки на сервисное обслуживание'
    summary_template = "{{ flow_class.process_title }} {{ process.request.number }} - {{ process.request.status }}"

    start = (
        flow.Start(sd_views.servicerequest_start_view, task_title="Регистрация заявки на сервисное обслуживание")
            .Permission(auto_create=True)
            .Next(this.implementation)
    )

    escalation = (flow.View(sd_views.incident_escalation, task_title='Эскалация')
                  .Permission(auto_create=True)
                  .Next(this.check_answer))

    # implementation = flow.View(sd_views.incident_implementation, task_title='Выполнение') \
    #     .Permission('can_implementation_servicerequestprocess_common', help_text='выполнение уровня ВСЕ',
    #                 auto_create=True) \
    #     .Permission('can_implementation_servicerequestprocess_developer', help_text='выполнение уровня РАЗРАБОТЧИКИ',
    #                 auto_create=True) \
    #     .Permission('can_implementation_servicerequestprocess_methodologist', help_text='выполнение уровня МЕТОДОЛОГИ',
    #                 auto_create=True) \
    #     .Permission(escalation_level_permission_create) \
    #     .Next(this.check_answer)
    implementation = ExtendedFlowView(sd_views.incident_implementation, task_title='Выполнение') \
        .EscalationLevelBasedPermissions(escalation_level_permission_create).Next(this.check_answer)


    check_answer = (flow.Switch(task_title='Проверка состояния')
                    .Case(this.close, lambda act: mycheck(act, 'solved'))
                    .Case(this.cc_notification, lambda act: mycheck(act, 'canceled'))
                    .Case(this.cc_notification, lambda act: mycheck(act, 'closed'))
                    .Case(this.escalation, lambda act: mycheck(act, 'returned'))
                    .Default(this.implementation))

    close = (flow.View(sd_views.incident_close_view, task_title='Закрытие')
             .Permission(auto_create=True)
             .Next(this.check_answer))

    another_end = flow.End()

    cc_notification = (flow.Handler(this.cc_notification_handler, task_title='Уведомление').Next(this.end))

    def cc_notification_handler(*args, **kwargs):
        print("Send email ok!")

    end = flow.End()


def mycheck(act, cond):
    return act.process.request.status.code == cond


@frontend.register
class IntroFlow(Flow):
    process_class = IntroProcess
    process_title = 'Обращение'
    process_description = u'Регистрация ображения в службу поддержки'

    start = (flow.Start(sd_views.intro_start_view, task_title="Регистрация обращения")
             .Permission(auto_create=True)
             .Next(this.end))
    make_request = ()
    end = flow.End()


def check_all_test_passed(activation):
    print("check test")
    all_passed = True
    for pt in activation.process.document.perfomancetest_set.all().iterator():
        all_passed = pt.passed
        if not all_passed:
            break
    return all_passed


@frontend.register
class DevelopmentFlow(Flow):
    process_class = DevelopmentProcess
    process_title = 'Разработка'
    process_description = 'Разработка нового функционала'

    start = (flow.Start(sd_views.development_start_view, task_title='Оформление задачи')
             .Permission(auto_create=True)
             .Next(this.development))
    development = (flow.View(sd_views.development, task_title='разработка')
                   .Permission(auto_create=True)
                   .Next(this.testing))
    testing = (flow.View(sd_views.perfomance_testing, task_title='тестирование')
               .Permission(auto_create=True)
               .Next(this.check_testing))
    check_testing = (flow.If(lambda act: check_all_test_passed(act))
                     .Then(this.close)
                     .Else(this.development))
    close = (flow.View(UpdateProcessView, task_title='закрытие разработки')
             .Permission(auto_create=True)
             .Next(this.end))

    end = flow.End()
