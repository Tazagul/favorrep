from django.apps import AppConfig


class ServiceDeskConfig(AppConfig):
    name = 'service_desk'
