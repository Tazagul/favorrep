from django.contrib import admin

# Register your models here.
from bpm.models import Status
from core.admin import ReferenceBaseAdmin
from service_desk.models import Customer, Incident, Intro, AccessObject, ServiceObject, Tracker, EscalationLevel


class CustomerAdmin(ReferenceBaseAdmin):
    list_display = ('code', 'name', 'branch', 'blocked')

    def get_queryset(self, request):
        return Customer.all_customers

admin.site.register(Status, ReferenceBaseAdmin)
admin.site.register(Intro, admin.ModelAdmin)
admin.site.register(Customer, CustomerAdmin)
admin.site.register(Incident, admin.ModelAdmin)
admin.site.register(AccessObject, ReferenceBaseAdmin)
admin.site.register(ServiceObject, ReferenceBaseAdmin)
admin.site.register(Tracker, ReferenceBaseAdmin)
admin.site.register(EscalationLevel, ReferenceBaseAdmin)
