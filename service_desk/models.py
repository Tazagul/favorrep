from django.contrib.auth.models import User
from django.db import models
# Create your models here.
from django.template import Template, Context
from model_utils.managers import InheritanceManager
from viewflow.models import Process

from bpm.models import Status
from core import auto_number
from core.models import AbstractBranchedModel, Reference  # , Attach
from django.utils.translation import ugettext_lazy as _


# VersionedObject, Version, Document, \

class HelloWorldProcess(Process):
    text = models.CharField(_('text'),max_length=150)
    approved = models.BooleanField(_('approved'),default=False)

    # class Meta:
    #     verbose_name=_('hello world process')
    #     verbose_name = _('hello world processes')


class Tracker(Reference):
    pass

    class Meta:
        verbose_name=_('tracker')
        verbose_name_plural=_('trackers')


class ActiveObjects(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(blocked=False)




class Customer(Reference):
    blocked = models.BooleanField(_('blocked'),default=False)
    objects = ActiveObjects()
    all_customers = models.Manager()

    class Meta:
        verbose_name=_('customer')
        verbose_name_plural=_('customers')

    def __str__(self):
        return self.name


class AbstractDocument(AbstractBranchedModel):
    class Meta:
        abstract = True
        # verbose_name=_('abstract document')

    number = models.CharField(_('number'),max_length=255)
    on_date = models.DateTimeField(_('on date'),auto_now_add=True)

    def __str__(self):
        return self.number and self.number or ("Документ без номера от %s" % self.on_date)


class ServiceObject(Reference):
    pass

    class Meta:
        verbose_name=_('service object')
        verbose_name_plural=_('service objects')


class AccessObject(Reference):
    pass

    class Meta:
        verbose_name=_('access object')
        verbose_name_plural=_('access objects')


@auto_number
class Intro(AbstractDocument):
    """
    документ Обращение
    """

    class Meta:
        verbose_name = u'Обращение'
        verbose_name_plural = u'Обращения'
        ordering = ['-on_date']

    customer = models.ForeignKey(Customer, null=True, blank=True,verbose_name=_('customer'))
    description = models.TextField(_('description'))
    answered = models.BooleanField(_('answered'),default=True)
    trackers = models.ManyToManyField(Tracker,verbose_name=_('trackers'))

    def __str__(self):
        short_desc = self.description[:45] + (self.description[45:] and "..")
        return "№{doc_number} от {doc_date}({customer}: {short_desc})".format(
            doc_date=self.on_date.strftime("%Y-%m-%d %H:%M"),
            doc_number=self.number,
            customer=self.customer,
            short_desc=short_desc)


class IntroProcess(Process):
    class Meta:
        verbose_name = "Процесс обращение"
        verbose_name_plural = "Процессы обращения"

    document = models.ForeignKey(Intro,verbose_name=_('document'))


class Request(AbstractDocument):
    objects = InheritanceManager()
    CATEGORY_CHOICES = (('INCIDENT', 'Инцидент'), ('SERVICE', 'Запрос на обслуживание'), ('ACCESS', 'Запрос на доступ'))
    LEVEL = (('HIGH', 'Высокий'), ('MIDDLE', 'Средний'), ('LOW', 'НИЗКИЙ'))
    PRIORITY_LEVEL = (('CRITICAL', 'Критический'), ('HIGH', 'Высокий'), ('MIDDLE', 'Средний'), ('LOW', 'НИЗКИЙ'),
                      ('PLAN', 'Планирование'))
    DEFAULT_CATEGORY = 'INCIDENT'
    intro = models.ForeignKey(Intro, null=True, blank=True,verbose_name=_('intro'))
    category = models.CharField(_('category'),max_length=255)
    customer = models.ForeignKey(Customer,verbose_name=_('customer'))
    customer_description = models.TextField(_('customer description'))
    employee_description = models.TextField(_('employee description'))
    impact = models.CharField(_('impact'),max_length=25, choices=LEVEL, default='LOW')
    urgency = models.CharField(_('urgency'),max_length=25, choices=LEVEL, default='LOW')
    priority = models.CharField(_('priority'),max_length=25, choices=PRIORITY_LEVEL, default='LOW')
    started_at = models.DateTimeField(_('started at'),auto_now_add=True)
    deadline = models.DateTimeField(_('deadline'))
    status = models.ForeignKey(Status, blank=True, null=True,verbose_name=_('status'))
    taken_by = models.ForeignKey(User,verbose_name=_('taken by'))

    # attaches = models.ManyToManyField(Attach)
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not self.pk and not self.category:
            self.category = self.DEFAULT_CATEGORY


@auto_number
class Incident(Request):
    def clean(self):
        if self.intro:
            print(self.intro.customer)
            self.customer = self.intro.customer
        if self.intro and not self.customer_description:
            self.customer_description = self.intro.description

    class Meta:
        verbose_name=_('incident')
        verbose_name_plural=_('incidents')


@auto_number
class ServiceRequest(Request):
    DEFAULT_CATEGORY = 'SERVICE'
    service_object = models.ForeignKey(ServiceObject,verbose_name=_('service object'))

    class Meta:
        verbose_name=_('service request')
        verbose_name_plural=_('service requests')


@auto_number
class AccessRequest(Request):
    DEFAULT_CATEGORY = 'ACCESS'
    access_object = models.ForeignKey(AccessObject,verbose_name=_('access object'))

    class Meta:
        verbose_name=_('access request')
        verbose_name_plural=_('access requests')


class RequestAnswer(models.Model):
    answer = models.TextField(_('answer'))
    request = models.ForeignKey(Request,verbose_name=_('request'))
    answered_at = models.DateTimeField(auto_now_add=True,verbose_name=_('answered at'))
    answered_person = models.ForeignKey(User,verbose_name=_('answered_person'))


class EscalationLevel(Reference):
    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        # TODO: переделать механизм регистрации прав при добавлении нового элемента справочника
        from django.core.management import call_command
        call_command('makemigrations')
        call_command('migrate')

    class Meta:
        verbose_name=_('escalation level')
        verbose_name_plural=_('escalation levels')


class RequestProcess(Process):
    class Meta:
        verbose_name = u'Процесс - Заявка'
        verbose_name_plural = u'Процессы - Заявки'

    request = models.ForeignKey(Request,verbose_name=_('request'))
    escalation_level = models.ForeignKey(EscalationLevel, blank=True, null=True,verbose_name=_('escalation level'))

    def summary(self):
        """Quick textual process state representation for end user."""
        if self.flow_class and self.flow_class.process_class == type(self):
            return Template(
                self.flow_class.summary_template
            ).render(
                Context({'process': self, 'flow_class': self.flow_class})
            )

        return "{} - {}".format(self.flow_class.process_title, self.request.status)

    def __str__(self):
        if self.flow_class:
            return '{} #{}'.format(self.flow_class.process_title, self.pk)
        return "<Process {}> - {}".format(self.pk, self.request.status)


class IncidentProcess(RequestProcess):
    class Meta:
        verbose_name = 'Процесс - Инцидент'
        verbose_name_plural = 'Процессы - Инцидент'


class AccessRequestProcess(RequestProcess):
    class Meta:
        verbose_name = 'Процесс - Предоставление доступа'
        verbose_name_plural = 'Процессы - Предоставление доступа'


class ServiceRequestProcess(RequestProcess):
    class Meta:
        verbose_name = 'Процесс - Сервисное обслуживание'
        verbose_name_plural = 'Процессы - Сервисное обслуживание'


@auto_number
class TDA(AbstractDocument):
    intro = models.ForeignKey(Intro, blank=True, null=True,verbose_name=_('intro'))
    info = models.TextField(_('info'))

    def __str__(self):
        return "№{doc_number} от {doc_date}".format(
            doc_date=self.on_date.strftime("%Y-%m-%d %H:%M"),
            doc_number=self.number)


class DevelopmentProcess(Process):
    class Meta:
        verbose_name = 'Процесс - разработка'
        verbose_name_plural = 'Процессы - разработки'

    document = models.ForeignKey(TDA,verbose_name=_('document'))


class PerfomanceTest(models.Model):
    """тест разработки"""
    passed = models.BooleanField(default=False, verbose_name='Пройдено?')
    info = models.TextField(verbose_name='Условия тестирования')
    result = models.TextField(verbose_name='Описание резульатата тестирования')
    tda = models.ForeignKey(TDA)
