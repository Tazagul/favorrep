from django.contrib.auth.models import User
from django.test import TestCase

# Create your tests here.
from service_desk.flows import AccessRequestFlow


class AccessRequestViewTests(TestCase):
    def test_accessrequest_startview_200(self):
        url = '/workflow/service_desk/accessrequest/start/'
        password = 'superPass123'
        my_admin = User.objects.create_superuser('admin', 'myemail@test.com', password)
        self.client.login(username=my_admin.username, password=password)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, '%s returns %s' % (url, response.status_code))
