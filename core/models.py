from django.contrib.auth.models import User
from django.db import models
from django.utils.datetime_safe import date
from django.utils.translation import ugettext_lazy as _


class Branch(models.Model):
    """
    справочник филиалов
    """
    name = models.CharField(_('name'), max_length=255)
    short = models.CharField(_('short'), max_length=255)
    is_blocked = models.BooleanField(_('is blocked'), default=False)

    class Meta:
        verbose_name = _("branch")
        verbose_name_plural = _("branches")

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name


class AbstractBranchedModel(models.Model):
    """
    абстрактный класс модели для разделения по предприятиям
    """
    class Meta:
        abstract = True
        #verbose_name=_('abstract branched model')
        #verbose_name_plural=_('abstract branched models')

    branch = models.ForeignKey(Branch, verbose_name=_("branch"))


class ObjectType(AbstractBranchedModel):
    """
    справочник типов объектов
    """
    code = models.CharField(_('code'),max_length=255)
    name = models.TextField(_('name'))
    klass = models.CharField(_('class'),max_length=255)
    parent = models.ForeignKey('self', null=True, blank=True,verbose_name=_('parent'))

    class Meta:
        verbose_name=_('object type')
        verbose_name_plural=_('object types')


class Numerator(AbstractBranchedModel):
    """
    класс номератора докумнтов
    """
    doc_type = models.ForeignKey(ObjectType, blank=True, null=True,verbose_name=_('doc type'))
    klass = models.CharField(_('class'),max_length=255)
    number = models.PositiveIntegerField(_('number'),default=0)
    capacity = models.PositiveSmallIntegerField(_('capacity'),default=7)
    year = models.PositiveSmallIntegerField(_('year'),default=date.today().year, blank=True, null=True)

    class Meta:
        verbose_name = u'Номератор документа'
        verbose_name_plural = u'Номераторы документов'
        unique_together = ('branch', 'klass', 'year')

    def __unicode__(self):
        return self.branch + u" " + self.get_doc_type_display() + u"(" + self.number + u")"


class Document(AbstractBranchedModel):
    """
    базовый класс для документов
    """
    class Meta:
        verbose_name = u'Документ'
        verbose_name_plural = u'Документы'

    document_id = models.AutoField(_('document id'),primary_key=True)
    number = models.CharField(max_length=255, blank=True, verbose_name=_('number'))
    on_date = models.DateField(auto_now_add=True, verbose_name=_('on date'))
    author = models.ForeignKey(User, blank=True, null=True, verbose_name=_('author'))


class Reference(AbstractBranchedModel):
    """
    базовый класс для справочников
    """
    class Meta:
        verbose_name = u'Справочник'
        verbose_name_plural = u'Справочники'

    code = models.CharField(_('code'),max_length=255)
    name = models.CharField(_('name'),max_length=255)
    description = models.TextField(_('description'))

    def __str__(self):
        return self.name

class Attach(models.Model):
    title = models.CharField(_('title'),max_length=255)
    file = models.FileField(_('file'),upload_to='attachs/%Y/%m/%d')

    class Meta:
        verbose_name=_('attach')
        verbose_name_plural=_('attaches')

#
# MyDynRef = type('MyDynRef', (Reference,), {
#     'first_name': models.CharField(max_length=255),
#     'last_name': models.CharField(max_length=255),
#     '__module__': 'core'
# })
