from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render

from core.forms import AttachForm
from core.models import Attach


@login_required
def attach_upload(request):
    if request.POST:
        form = AttachForm(request.POST, request.FILES)
        if form.is_valid():
            f = form.save()
        else:
            print('has error!')
    else:
        form = AttachForm()
    return render(request, 'file_form.html', context={'form': form, 'files': Attach.objects.all().iterator()})


