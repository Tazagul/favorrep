default_app_config = 'core.apps.CoreConfig'


def auto_number(cls):
    """Register a model to set auto number."""
    from django.apps import apps
    apps.get_app_config('core').add_autonumber(cls)
    return cls
