from django.contrib import admin

# Register your models here.
from core.models import Branch, Numerator, ObjectType, Reference


class ReferenceBaseAdmin(admin.ModelAdmin):
    list_display = ('code', 'name', 'branch')
    list_filter = ('branch',)
    search_fields = ('code', 'name')


admin.site.register(Branch, admin.ModelAdmin)
admin.site.register(Numerator, admin.ModelAdmin)
admin.site.register(ObjectType, admin.ModelAdmin)
admin.site.register(Reference, ReferenceBaseAdmin)