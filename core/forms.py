from django import forms

from core.models import Attach


class AttachForm(forms.ModelForm):
    title = forms.CharField()
    file = forms.FileField()

    class Meta:
        model = Attach
        fields = ('title', 'file')
