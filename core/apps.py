from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = 'core'

    def ready(self):
        import core.signals

    def __init__(self, app_name, app_module):  # noqa D102
        super().__init__(app_name, app_module)
        self._need_auto_number = []

    def add_autonumber(self, cls):
        if cls._meta.label not in self._need_auto_number and hasattr(cls, 'number'):
            self._need_auto_number.append(cls._meta.label)
