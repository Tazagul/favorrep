import datetime

from django.db.models.signals import pre_save

from .models import Numerator


def inherited_receiver(signal, sender, **kwargs):
    parent_cls = sender

    def wrapper(func):
        def childs_receiver(sender, **kw):
            child_cls = sender
            if issubclass(child_cls, parent_cls):
                func(sender=child_cls, **kw)

        signal.connect(childs_receiver, **kwargs)
        return childs_receiver

    return wrapper


def set_doc_num_before_saving(sender, **kwargs):
    # print("signal!")
    from django.apps import apps
    core_cfg = apps.get_app_config('core')
    doc = kwargs['instance']
    if sender._meta.label in core_cfg._need_auto_number and hasattr(doc, 'number') and hasattr(doc, 'on_date') \
            and hasattr(doc, 'branch') and not doc.number:
        if doc.on_date is None:
            year = datetime.datetime.now().year
        else:
            year = doc.on_date.year
        numerator, error = Numerator.objects.select_for_update() \
            .get_or_create(klass=sender._meta.label, year=year, branch=doc.branch)
        print(error)
        numerator.number += 1
        numerator.save()
        # print()
        doc.number = str(year) + '-' + '0' * (numerator.capacity - len(str(numerator.number))) + str(numerator.number)


pre_save.connect(set_doc_num_before_saving)
