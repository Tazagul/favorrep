from django.conf.urls import url, include
from django.contrib import admin
from django.views import generic
from material.frontend import urls as frontend_urls

# from core.views import create_custom_model, MyDynRefView
from core.views import attach_upload

urlpatterns = [
    url('^file/upload/$', attach_upload, name='attach-upload')
]
